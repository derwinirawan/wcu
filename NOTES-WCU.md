---
title: "Catatan tentang pemeringkatan perguruan tinggi dunia"
author: "Dasapta Erwin Irawan"
output:
  word_document:
    toc: yes
    toc_depth: '3'
  pdf_document:
    toc: yes
    toc_depth: '3'
  html_document:
    number_sections: yes
    toc: yes
    toc_depth: 3
affiliation: Institut Teknologi Bandung
---

# Catatan tentang pemeringkatan perguruan tinggi  dunia

Oleh: 
Dasapta Erwin Irawan ([ORCID](https://orcid.org/0000-0002-1526-0863))
Institut Teknologi Bandung

# Referensi

Saya sengaja meletakkan bagian referensi di bagian awal agar pembaca mengetahui bahwa referensi tentang WCU telah tersedia dalam berbagai bentuk, tidak hanya makalah ilmiah tertinjau saja. Di saat awal ini pula, saya ingin membuka cakrawala pembaca tentang referensi. 

Jadi berikut ini adalah referensi yang saya gunakan.

1. https://www.youtube.com/watch?v=Vf6wrANVMh4
2. https://www.youtube.com/watch?v=D7Ccw2ICRvE 
3. https://www.youtube.com/watch?v=AI25dx3B-9I
4. https://www.inqaahe.org/guidelines-good-practice
5. https://youtu.be/n5dP3Aslq24
6. [ARWU](http://www.shanghairanking.com/)
7. [Leiden Ranking](https://www.leidenranking.com/)
8. [THES](https://www.timeshighereducation.com/world-university-rankings/2020/world-ranking#!/page/0/length/25/sort_by/rank/sort_order/asc/cols/stats)
9. [QS](https://www.topuniversities.com/)
10. [UMULTIRANK](https://www.umultirank.org/)
11. [The dubious practice of university rankings | Elephant's in the lab](https://elephantinthelab.org/the-accuracy-of-university-rankings-in-a-international-perspective/) 
12. [The still unsustainable goal of university ranking | Prof Stephen Curry's blog](http://occamstypewriter.org/scurry/2020/04/26/still-unsustainable-university-rankings/)
13. https://www.youtube.com/watch?v=N6etSuned3w
14. [UI Green Metric](https://id.wikipedia.org/wiki/Peringkat_universitas_dunia_UI_GreenMetric), karena website resminya bermasalah

## Tentang pemeringkatan

### Apa saja

Pemeringkatan yang paling banyak digunakan ada empat, khususnya mengagregasi indikator terkait pendidikan, penelitian, dan hubungan dengan pihak eksternal:

* [ARWU](http://www.shanghairanking.com/ARWU2019.html)
* [THES](https://www.timeshighereducation.com/)
* [QS](https://www.topuniversities.com/qs-world-university-rankings)
* [Leiden](https://www.leidenranking.com/)

Ada pendatang (baru) dengan indikator yang sangat berbeda, yakni kinerja terkait lingkungan 

* [UI Green Metric](https://id.wikipedia.org/wiki/Peringkat_universitas_dunia_UI_GreenMetric)
* [THES SDG Impact Ranking](https://www.timeshighereducation.com/rankings/impact/2019/partnerships-goals#!/page/0/length/25/sort_by/rank/sort_order/asc/cols/undefined)

### Pemeringkatan dengan indikator konvensional

Keempatnya mengukur indikator-indikator terkait pengajaran (teaching), penelitian (research), dan hubungan eksternal (external relationship) yang masing-masing dihubungkan dengan reputasi internasional.

#### ARWU

Dalam [laman resminya](One unique feature in the methodology of ShanghaiRanking's ARWU and  subject rankings is the use of Award indicator, such as counting the  universities' staff winning Nobel Prize in Physics, Chemistry,  Physiology/Medicine, Economics and 				Fields Medals in Mathematics.), ARWU menyampaikan keunikan pemeringkatan sbb. Kalau dilihat sebenarnya tidak ada yang unik dengan basis pemeringkatannya, yang masih mengutamakan pencapaian individual.

> One unique feature in the methodology of ShanghaiRanking’s ARWU and  subject rankings is the use of Award indicator, such as counting the  universities’ staff winning Nobel Prize in Physics, Chemistry,  Physiology/Medicine, Economics and Fields Medals in Mathematics.

Agar dapat masuk ke pemeringkatan ARWU, kriteria seleksi awalnya adalah jumlah minimum publikasi yang terdata di Web of Science. [Batasan jumlah minimum publikasi akan berbeda-beda untuk setiap bidang ilmu](http://www.shanghairanking.com/shanghairanking-subject-rankings/Methodology-for-ShanghaiRanking-Global-Ranking-of-Academic-Subjects-2019.html). 

ARWU menggunakan [beberapa indikator di bawah ini](http://www.shanghairanking.com/shanghairanking-subject-rankings/Methodology-for-ShanghaiRanking-Global-Ranking-of-Academic-Subjects-2019.html). Daftar-daftar yang diakui dan digunakan dalam pemeringkatan ada [di sini](http://www.shanghairanking.com/shanghairanking-subject-rankings/Methodology-for-ShanghaiRanking-Global-Ranking-of-Academic-Subjects-2019.html).

| Indicators | Definition                                                   |
| ---------- | ------------------------------------------------------------ |
| PUB        | PUB is the number of papers authored by an institution in an  Academic Subject during the period of 2013-2017. Only papers of  'Article' type are considered. **Data are collected from Web of Science  and InCites**. `Ringkasan: PUB = jumlah makalah dalam suatu bidang ilmu yang ditulis oleh staf lembaga pada periode tertentu. Data dari WoS dan InCites ` |
| CNCI       | Category Normalized Citation Impact (CNCI)  is **the ratio of citation of papers published by an institution in an Academic Subject during the period of 2013-2017 to the average citations of papers in the same category, of the same year and same type**. A CNCI  value of 1 represents world-average performance while a value above 1  represents performance above the world average. Only papers of 'Article' type are considered. **Data are collected from InCites database**. `Ringkasan: CNCI = jumlah sitasi dari makalah dalam suatu bidang ilmu yang ditulis oleh staf lembaga pada periode tertentu. Data dari InCites ` |
| IC         | International collaboration (IC) is **the number of publications that have been found with at least two different countries in addresses of the authors divided by the total number of  publications in an Academic Subject for an institution during the period of 2013-2017**. Only papers of ‘Article’ type are considered. **Data are  collected from InCites database**. `Ringkasan: IC = (jumlah makalah pada bidang ilmu tertentu yang ditulis oleh staf lembaga dan penulis dari negara lain) / (jumlah publikasi pada bidang ilmu dari suatu lembaga pada periode tertentu). Data dair InCites ` |
| TOP        | **TOP is the number of papers published in  Top Journals in an Academic Subject for an institution during the period of 2013-2017. Top Journals are identified through ShanghaiRanking’s  Academic Excellence Survey or by Journal Impact Factor**. In 2019, 134 top journals selected by the Survey are used in rankings of 45 Academic  Subjects. In Computer Science & Engineering, 17 selected top  conferences are also taken into account this year. The list of the top  journals and conferences can be found [**here**](http://www.shanghairanking.com/subject-survey/top-journals.html). For Academic Subjects that do not have journals identified by the  Survey, the JCR top 20% journals are used. Top 20% journals are defined  as their Journal Impact Factors in the top 20% of each Web of Science category according to Journal  Citation Report (JCR) 2017, and then aggregated into different Academic  Subjects. Only papers of 'Article' type are considered for this  indicator. But in the subject of Pharmacy & Pharmaceutical Sciences, both "Article" and "Review" are counted because only one journal in  this subject was selected as Top journal and it mainly publishes  reviews. `Ringkasan: TOP = jumlah makalah yang ditulis oleh staf lembaga dan terbit di jurnal top pada bidang ilmu tertentu. Daftar jurnal top didapatkan dari survey responden. Untuk bidang ilmu yang jurnalnya tidak ada dalam daftar hasil survey, maka data diambil dari JCR (JIF) dari WoS` |
| AWARD      | **AWARD refers to the total number of the staff of an institution wining a significant award in an Academic Subject since 1981**. Staff is  defined as those who work full-time at an institution at the time of  winning the prize. If a researcher retired at the time of winning the  award, we count the institution where the researcher’s last full-time  academic position was at. The significant awards in each subject are  identified through [**ShanghaiRanking’s Academic Excellence Survey**](http://www.shanghairanking.com/subject-survey/index.html).  The list of the significant awards in each subject can be viewed [**here**](http://www.shanghairanking.com/subject-survey/awards.html).   If a winner is affiliated with more than one institution at the time of winning the award, each institution is assigned the reciprocal of the  number of institutions. If the award is awarded to more than one winner  in one year, weights are set for winners according to their proportion  of the prize. Different weights are set according to the periods of  winning the prizes. **The weight is 100% for winners in 2011-2017, 75% for winners in 2001-2010, 50% for winners in 1991-2000, and 25% for winners in 1981-1990**.  Nobel Prize in Physiology or Medicine is selected for  Biological Sciences, Human Biological Sciences, Clinical Medicine and  Pharmacy & Pharmaceutical Sciences. Winners of this award are  assigned to one or more subjects according to the topics of their  recognized contributions. If a contribution belongs to more than one  subject, the winner will be counted once for each relevant subject. `Ringkasan: AWARD = jumlah penghargaan yang diterima staf berdasarkan hasil survei. Penghargaan Nobel adalah yang diutamakan` |



#### THES

Agar bisa masuk ke dalam [lingkungan survey THES](https://www.timeshighereducation.com/how-participate-times-higher-education-rankings), berikut syaratnya:

* Publish a sufficient number of academic papers over a five-year period – this threshold is currently set at 1,000 papers, kecuali untuk negara-negara Amerika Latin yang hanya perlu menerbitkan 200 makalah (`Institutions are eligible for inclusion in the Latin America table if they have published 200 papers in the past five years.`)
* Teach undergraduates
* Work across a range of subjects

THES menggunakan [beberapa indikator ini](https://www.timeshighereducation.com/world-university-rankings/world-university-rankings-2019-methodology):

* Teaching (the learning environment) – 30%
    * Reputation survey: 15% (`berdasarkan responden terpilih, kriteria belum diketahui dengan pasti`)
    * Staff-to-student ratio: 4.5%
    * Doctorate-to-bachelor’s ratio: 2.25%
    * Doctorates-awarded-to-academic-staff ratio: 6%
    * Institutional income: 2.25%
* Research (volume, income and reputation) – 30%
    * Reputation survey: 18% (`berdasarkan responden terpilih, kriteria belum diketahui dengan pasti`)
    * Research income: 6%
    * Research productivity: 6%
* Citations (research influence) – 30% (`berdasarkan data Scopus`)
* International outlook (staff, students, research) – 7.5%
    * Proportion of international students: 2.5%
    * Proportion of international staff: 2.5%
    * International collaboration: 2.5%
* Industry income (knowledge transfer) – 2.5%

#### QS

QS tidak menampilkan kriteria inkluasi di depan. Perguruan tinggi yang berminat hanya diminta untuk menghubungi QS. 
[QS menggunakan beberapa indikator ini](https://www.topuniversities.com/qs-world-university-rankings/methodology):

* Academic Reputation (40%)
* Employer Reputation (10%)
* Faculty/Student Ratio (20%)
* Citations per faculty (20%)
* International Faculty Ratio (5%)
* International Student Ratio (5%)


#### Leiden

Pemeringkatan Leiden sepenuhnya bersandar kepada data publikasi dengan mamanfaatkan  [basis data WoS dan turunan-turunannya](https://www.leidenranking.com/information/indicators). Jadi makalah yang diperhitungkan harus masuk ke dalam pengindeks itu, juga jumlah sitasinya. Selain itu Leiden yang mensyaratkan bahwa publikasi (**hanya yang berjenis artikel/_peer reviewed article_ dan ulasan/_peer reviewed review_**) harus ditulis dalam Bahasa Inggris dan telah masuk ke dalam **basis data inti WoS**, serta mengeluarkan makalah anonim dan makalah yang telah ditarik (_retracted_). Agar dapat masuk dalam [basis data jurnal ini](https://www.leidenranking.com/Content/CWTS%20Leiden%20Ranking%202019%20-%20Core%20and%20non-core%20journals.xlsx), sebuah jurnal harus memiliki kriteria: memiliki daftar makalah dengan penulis yang berasal dari berbagai negara dan memiliki kaitan sitasi ke jurnal lain yang telah masuk ke dalam daftar inti. Pemeringkatan ini tidak memperhitungkan 

Walaupun indikator yang digunakan oleh Pemeringkatan Leiden bertujuan untuk memperlihatkan produktivitas, kolaborasi, interaksi dengan industri, keterbukaan dan keseimbangan gender, tetapi yang digunakan adalah makalah ilmiah dengan memperhatikan:

* jumlah publikasi di tiap perguruan tinggi: makin banyak, makin bagus;
* jumlah publikasi yang berstatus akses terbuka (_open access_): makin banyak, makin bagus.
* jumlah publikasi menurut bidang ilmu: makin banyak, makin bagus;
* jumlah publikasi menurut jumlah penulis yang berasal dari organisasi yang berbeda: makin bervariasi, makin bagus;
* jumlah publikasi dari penulis perempuan: makin banyak, makin bagus;
* jumlah publikasi dengan penulis yang berasal dari sektor industri: makin banyak, makin bagus. Ini menunjukkan tingkat interaksi antara universitas dengan sektor industri; 
* jumlah publikasi dengan penulis yang berasal dari negara yang berbeda dan seberapa jauh jaraknya: makin banyak, makin bagus dan makin jauh jarak antar negara, makin bagus. Ini memperlihatkan tingkat internasionalisasi universitas.

### Pemeringkatan dengan indikator non-konvensional

#### UI Green Metric

Untuk UI Green Metric yang diukur adalah indikator terkait Kampus Hijau, yakni berbagai [upaya kampus untuk mengurangi jejak karbon dan resiko pemanasan global](https://id.wikipedia.org/wiki/Peringkat_universitas_dunia_UI_GreenMetric). Indikator-indikator yang digunakan sangat terkait dengan tata kelola lingkungan, dengan rincian sebagai berikut:

* Pengaturan lahan dan infrastruktur (SI) = bobot 15% 
  Berbagai pengaturan gedung dan lingkungan universitas dalam menuju lingkungan hijau. Informasi mengenai pengaturan lahan dan infrastruktur universitas akan memberikan informasi dasar mengenai pertimbangan universitas menuju lingkungan hijau. Indikator ini juga menunjukkan apakah suatu Universitas layak disebut sebagai kampus hijau. Tujuan dari indikator ini adalah agar universitas menyediakan ruang lebih untuk penghijauan dan keamanan lingkungan. 

* Energi dan perubahan iklim (EC) = bobot 21% 
  Perhatian universitas terhadap penggunaan energi, alternatif energi dan masalah perubahan iklim. Perhatian universitas terhadap masalah penggunaan energi dan perubahan iklim adalah indikator yang diberi persentase tinggi. Indikator ini memperhatikan penggunaan energi yang efisien, penggunaan listrik, program konservasi energi, dan lain-lain. Dengan indikator seperti ini diharapkan universitas bisa lebih efisien dalam menggunakan energi dan lebih peduli terhadap sumber energi alami. 

* Sampah (WS) = bobot 18% 
  Perlakuan dan daur ulang sampah dan limbah yang ada di universitas. Aktivitas pengolahan limbah dan daur ulang sampah memiliki faktor yang besar dalam lingkungan yang hijau. Indikator ini fokus terhadap program universitas dalam mengelola limbah yang dihasilkan, seperti daur ulang, pengolahan air organik, sistem pembuangan kotoran, dan aturan dalam penggunaan kertas dan plastik di universitas. 

* Air (WR) = bobot 10% 
  Program universitas untuk menghemat penggunaan air. Indikator penggunaan air di universitas bertujuan agar universitas bisa mengurangi penggunaan air, menambahkan program konservasi air, dan lain-lain. 

* Transportasi (TR) = bobot 18% 
  Program universitas untuk transportasi ramah lingkungan. Sistem transportasi memiliki peranan penting terkait emisi karbon dan polusi di universitas. Indikator transportasi menilai tentang peraturan universitas yang berkaitan dengan transportasi, seperti pembatasan kendaraan bermotor, penggunaan bus dan sepeda di universitas untuk mendorong lingkungan yang lebih sehat. 

* Edukasi (ED) = bobot 8% 
  Peranan Universitas dalam membina generasi hijau, dengan pendidikan, penelitian, dan aktivitas penunjang. Indikator ini lebih memfokuskan pada berbagai upaya universitas dalam menciptakan generasi baru yang lebih peduli terhadap keberlanjutan lingkungan hidup. 

#### THES Dampak SDG

THES SDG mengukur peran kampus dalam meningkatkan komponen SDG:

* 3. Good health and well being
* 4. Quality education
* 5. Gender equality
* 8. Decent work and economic growth
* 9. Industri innovations and infrastructure
* 10. Reduced inequalities
* 11. Sustainable cities and communities
* 12. Responsible consumption and production
* 13. Climate action
* 16. Peace, justice strong institutions

# Analisis singkat

## Transparansi

Mayoritas pemeringkatan tidak menyediakan data dan analisis secara transparan. Setidaknya, hanya Pemeringkatan Leiden yang menyatakan dengan jelas tautan ke [laman tentang data](https://www.leidenranking.com/information/data). Data inti dari seluruh pemeringkatan konvensional itu berasal dari basis data yang berbayar, yaitu Scopus dan WoS. Jadi pera pengguna harus memiliki akses ke kedua basis data itu untuk membuat ulang (_reproduce_) skor peringkatnya. Ini berarti membutuhkan dana lagi.  Pemeringkatan non-konvensional seperti UI Green Metric dan THES SDG juga sama kondisinya. 

### Apa pentingnya 

Yang sudah pasti tidak dilakukan adalah mencerminkan posisi peringkat dengan prestasi, apalagi sebagai basis penyusunan kebijakan penganggaran, misal: untuk menentukan jumlah anggaran bagi perguruan tinggi tertentu. Pada level indikator, perguruan tinggi dapat mengamati kinerjanya, walaupun tidak semuanya, karena akan berpotensi mengalihkan sebuah perguruan tinggi dari misi awalnya.  Indikator yang terkait dengan jumlah staf asingi dan mahasiswa asing, indikator yang berkaitan dengan jumlah publikasi dan jumlah sitasi.  


### Limitasi

Bila diperhatikan, maka limitasi untuk pemeringkatan konvensional adalah masalah diversitas. Seluruh pemeringkatan itu hanya menilai kinerja perguruan tinggi dari kriteria dan basis data yang terbatas. Sebagian besar malah melakukan diskriminasi terhadap bahasa selain Bahasa Inggris untuk alasan internasionalisasi. 

Untuk pemeringkatan non-konvensional, UI Green Metric mungkin adalah satu-satunya yang merekam kondisi suatu perguruan tinggi di luar jumlah publikasi dan sitasinya. Indikator-indikator yang digunakannya memang lebih riil, tetapi mungkin saja juga tidak dapat diikuti oleh perguruan tinggi yang masih baru berdiri dengan luasan lahan tertentu atau perguruan tinggi yang walaupun sudah berdiri lama dan stabil tapi berada pada lahan yang sempit. Kedua kondisi tersebut menghambat perguruan tinggi untuk berinovasi berkaitan dengan prinsip keberlanjutan lingkungan.

THES SDG juga mungkin akan memiliki keterbatasan untuk mengevaluasi perguruan tinggi di Asia, Asia Tenggara khususnya, atau negara-negara selatan (_global south_), karena di negara-negara tersebut kegiatan-kegiatan yang dilaksanakan masih jarang atau sangat sedikit yang dilebeli dengan SDG, misal: bila suatu perguruan tinggi mengadakan penelitian tentang bidang X, jarang dihubungkan atau diberi label terkait dengan SDG no A.  Begitu pula juga untuk publikasi, sangat sedikit jurnal ilmiah, yang terbit di luar dan dalam negeri yang meminta penulis memasukkan SDG yang relevan dengan makalahnya. Bisa jadi pengelola jurnal yang melakukan hal tersebut, tetapi pilihannya bisa jadi tidak sesuai dengan pilihan penulis (bila mereka ditanya). 

### Relevansi indikator peringkat vs visi dan misi perguruan tinggi (PT)

Bab ini menyusul akan saya uraikan satu persatu dengan pesan inti:

* Perguruan tinggi, kementerian, dan pemberi dana riset agar berhati-hati dalam menilai hasil pemeringkatan. Kritis terhadap data dan metode sangat diperlukan. Mayoritas pemeringkatan tidak menyediakan data mentah dan hasil perhitungannya. Beberapa daftar rujukan juga berasal dari penilaian yang subyektif berdasarkan pengisian kuesioner oleh responden..
* Mayoritas pemeringkatan mengklaim telah menggunakan indikator multidimensi. Pada level umum itu memang dilakukan, yakni dengan mencakup kegiatan bidang pendidikan, penelitian, dan interaksi dengan pihak industri. Tapi harus disadari bahwa indikator yang digunakan penuh dengan bias geografis dan bahasa. Pemeringkatan Leiden dalam hal ini hanya mendasarkan penilaian dari publikasi, serta UI Green Metric hanya menilai indikator terkait tata kelola lingkungan.
* Tidak menggunakan indikator-indikator yang jelas memiliki bias tinggi sebagai indikator penentu untuk merumuskan kebijakan. 


# Beberapa kutipan menarik

Berikut ini saya kutip beberapa pertanyaan menarik seputar pemeringkatan. Semua kutipan berasal dari orang asing, jadi kita tidak dapat menyebut mereka sebagai **kaum penghindar**.



>Research metrics are derived from data supplied by Elsevier. For each SDG, a specific query has been created that narrows the scope of the metric to papers relevant to that SDG. As with the World University Rankings, we are using a five-year window between 2013 and 2017. The only exception is the metric on patents that cite research under SDG 9, which relates to the timeframe in which the patents were published rather than the timeframe of the research itself. The metrics chosen for the bibliometrics differ by SDG and there are always at least two bibliometric measures used. [THES](https://www.timeshighereducation.com/university-impact-rankings-2019-methodology)



>So the question regarding “authority” is hard to answer. In my view there is no authoritative single source of information.  It is more important to look at several rankings, develop some understanding of the methodology used to create the ranking and then consider why I need the information to determine which ranking or rankings might be the most informative with respect to my needs. [University Rankings: How Important Are They?](https://www.forbes.com/sites/evacairns/2013/10/17/university-rankings-how-important-are-they-an-interview-with-nello-angerilli-avp-university-of-waterloo-canada/#5b1f3f0c7d3c)




>The Impact Rankings score participating universities on how well their activities contribute to the UN Sustainable Development Goals (SDGs), which range across issues such as poverty, gender equality, climate action, health and well-being, peace and justice. Although the compilation of the rankings is primarily motivated as a way to celebrate the real-world impact of what many universities do, a noble aspiration that I applaud, the core methodology remains unfit for purpose. At its centre, as with almost all rankings, there is an intellectual hollowness that undermines the whole project, and it is disappointing to see that the THE has yet to take responsibility for their methodological shortcomings. It is even more disappointing to see some universities abandon critical thinking in their rush to embrace the results. [The still unsustainable goal of university ranking | Prof Stephen Curry's blog](http://occamstypewriter.org/scurry/2020/04/26/still-unsustainable-university-rankings/)




>Rankings are not an appropriate method for assessing or comparing quality, or the basis for making strategic decisions by countries or universities. If a country or institution wishes to improve performance there are alternative methodologies and processes. Beware unintended consequences of simplistic approaches.[The dubious practice of university rankings](https://elephantinthelab.org/the-accuracy-of-university-rankings-in-a-international-perspective/)
