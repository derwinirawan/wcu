# World University Ranking

Repositori ini berisi berbagai hal terkait dengan Pemeringkatan Universitas Kelas Dunia atau _World Class University_. Di sini semaksimum mungkin saya akan mendeskripsikan beberapa tentang:

* Mengapa dan bagaimana pemeringkatan ini dimulai?
* Apa saja pemeringkatan yang saat ini digunakan di dunia?
* Apa saja indikator yang digunakannya?
* Siapa saja yang menggunakannya?
* Dan mengapa pemeringkatan tidak bersifat generik dan tidak dapat digunakan untuk menentukan arah pengembangan sebuah perguruan tinggi di Indonesia?

Di sini saya akan membahas: ARWU, THES, QS, SDG index dan Green index.

Semoga saya bisa obyektif dalam membuat ulasan ini, karena harus diakui saya bukan pengikut aliran pemeringkatan. 

Bismillahirrohmanirrohim.
 
